
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const base_url = process.env.VUE_APP_BASE_URL;

const module_scorecard = {

    namespaced: true,

    state: {
        staff_list: [],
        period_list: [],
        selected_staff: null,
        selected_period: 'W24'
    },

    mutations: {

        setPeriodList (state, payload) {
            state.period_list = payload;
        },

        setStaffList (state, payload) {
            state.staff_list = payload;
        },

        setSelectedStaff (state, payload) {
            state.selected_staff = payload;
        },

        setSelectedPeriod (state, payload) {
            state.selected_period = payload;
        }
    },

    actions: {

        loadStaffList (context) {

            let url = `${base_url}/staff`;

            fetch(`${url}`)
                .then(response => response.json())
                .then(data => context.commit('setStaffList', data));
        },

        loadPeriodList (context) {

            let url = `${base_url}/period`;

            fetch(`${url}`)
                .then(response => response.json())
                .then(data => context.commit('setPeriodList', data));
        }
    }
}

export default new Vuex.Store({

    state: {

        user_name: localStorage.getItem('user.name'),
        is_admin: localStorage.getItem('is_admin')
    },

    mutations: {

        updateUserName (state, payload) {
            state.user_name = payload;
        },

        updateIsAdmin (state, payload) {
            state.is_admin = payload;
        }
    },

    actions: {

    },

    modules: {

        scorecard: module_scorecard
    }
});
