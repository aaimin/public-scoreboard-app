
import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      component: Home,
      redirect: '/batch/metric-tracks/create'
    },
    {
      path: '/login',
      meta: { hide_toolbar: true },
      component: () => import(
        './views/Login.vue'
      )
    },
    {
      path: '/batch/metric-tracks/create',
      component: () => import('./views/BatchMetricTracksForm.vue')
    },
    {
      path: '/accountability',
      component: () => import('./views/Accountability.vue')
    },
    {
      path: '/metric-connector',
      component: () => import('./views/MetricConnector.vue')
    },
    {
      path: '/report-scoreboard-submission',
      component: () => import('./views/ReportScoreboardSubmission.vue')
    },
    {
      path: '/report-progress-summary',
      component: () => import('./views/ReportProgressSummary.vue')
    },
    {
      path: '/scoreboard',
      component: () => import(
        './views/Scoreboard'
      )
    },
    {
      path: '/simple-scoreboard',
      component: () => import('./views/SimpleScoreboard.vue')
    },
    {
      path: '/scoreboard-review',
      component: () => import('./views/ScoreboardReview.vue')
    },
    {
      path: 'user-setting',
      component: () => import('./views/UserSetting.vue')
    },
    {
      path: '/print-scoreboard',
      component: () => import(
        './views/PrintScoreboard'
      )
    },
    
  ]
});

router.beforeEach(function(to, from, next) {

  if (to.path == '/login') {
    return next();
  }

  let token = localStorage.getItem('token');

  // Temporary solution to alert user before proceed to other page.
 
  if (globalMetricsEdited) {

    return Swal.fire({
      title: 'Data may be lost',
      text: 'Please save before you proceed to other page.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Proceed',
      cancelButtonText: 'Back',
      reverseButtons: true,
      customClass: {
        confirmButton: 'bg-dark',
        cancelButton: 'bg-info'
      }
    }).then((result) => {
      if (result.value) { 
        globalMetricsEdited = false;
        next();
      }
    });
  }

  if (token) {
    return next();
  }

  localStorage.setItem('route.location', to.fullPath);

  return next('/login');
});

export default router;
