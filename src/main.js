
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import 'bootstrap';

import 'bootstrap/dist/css/bootstrap.min.css';
import '@/assets/the-admin-1.1.1/app.css';
import '@/assets/themify-icons/themify-icons.css';
import '@/assets/tailwind.css';
import '@/assets/fix.css';

import '@/helpers/global-plugins';

import LogRocket from 'logrocket';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

Vue.filter('capitalize',
    value => value ? value.toUpperCase() : value
);

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app');

LogRocket.init('msflwq/pn-scoreboard');

LogRocket.identify(localStorage.getItem('user.id'), {
    name: localStorage.getItem('user.name'),
    email: localStorage.getItem('user.email'),
});