
import axios from 'axios';
import jQuery from 'jquery';
import Swal from 'sweetalert2';

window.api_url = process.env.VUE_APP_BASE_URL;

window.axios = axios;

window.$ = jQuery;
window.jQuery = jQuery;

window.Swal = Swal;

window.globalMetricsEdited = false;